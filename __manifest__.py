# -*- coding: utf-8 -*-
{
    'name': "Module Fouta communication",

    'summary': """ Module principal du projet Fouta communication """,

    'description': """
    Module principal du projet Fouta communication
    """,
    'sequence': 1,

    'author': "KOURA TECHNOLOGIES",
    'website': "http://www.kouratech.com",

    'category': 'finance',
    'version': '1.0',

    'depends': ['base'],

    'data': [

    ],
    'demo': [

    ],
    'application': True,
}
